#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
@author: m

# requirements

* python3
* GitPython
"""

import requests, sys, git, os, json
from requests.auth import HTTPBasicAuth

GITHUB_USER = os.environ.get('GITHUB_USER')
GITHUB_PASSWORD = os.environ.get('GITHUB_PASSWORD')

VERBOSE = True

def check_env():
    return GITHUB_USER is None and GITHUB_PASSWORD is None

def debug(string):
    if VERBOSE:
        print(string)

def get_all_branches(name):
    response = requests.get('https://api.github.com/repos/' + name + '/git/refs')
    return response.json()

def delete_branches(origin):
    refs = get_all_branches(origin['full_name'])
    debug(origin['full_name'])
    debug('start deleting ' + str(len(refs)) + ' branches')
    for ref in refs:
        response = requests.delete('https://api.github.com/repos/' + origin['full_name'] + '/git/' + ref['ref'],  auth=HTTPBasicAuth(GITHUB_USER, GITHUB_PASSWORD))
        print(response)


if __name__ == '__main__':
    if check_env():
        print("please export ENV variables and run")
        print("./delete_all_github_branches.py")
        sys.exit(1)
    
    debug("delete branches from " + GITHUB_USER)
    page = 1
    while True:
        response = requests.get("https://api.github.com/users/" + GITHUB_USER + "/repos?page=" + str(page) + "&per_page=100")
        repos = response.json()
        if len(repos) > 0:
            for repo in repos:
                delete_branches(repo)
            page += 1
        else:
            break
