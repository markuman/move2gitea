# move2gitea

bye bye microsoft

This python script will migrate your github.com projects into a gitea instance.  
When you just want to clone all your repositories and starred repositories to a local filedrive, try [mygithub](https://git.osuv.de/m/mygithub)

## mirrors

* origin: https://git.osuv.de/m/move2gitea
* CI/CD Mirror: https://gitlab.com/markuman/move2gitea


## how to - migrate your repositories from github.com to a gitea host

```
# install dependencies if not done yet
sudo pip install gitpyton requests

# export GITEA env variables
export GITEA_HOST=git.osuv.de
export GITEA_UID=1 # the UID of your user
export GITEA_TOKEN=abc123 # create an access token for your user

# export github user and password
export GITHUB_USER=markuman
export GITHUB_PASSWORD=xxx # yout github.com password, when you need to migrate private repositories

# run
./move2gitea.py
```

## mirror your github starred project to a gitea host

```
# install dependencies if not done yet
sudo pip install gitpyton requests

# export GITEA env variables
export GITEA_HOST=git.osuv.de
export GITEA_UID=1 # the UID of your user
export GITEA_TOKEN=abc123 # create an access token for your used

# export github user and password
export GITHUB_USER=markuman

# run
./mirror_github_stars.py
```