#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
@author: m

# requirements

* python3
* GitPython
* requests
"""

import requests, sys, git, os, json

GITEA_HOST = os.environ.get('GITEA_HOST')
GITEA_UID = int(os.environ.get('GITEA_UID'))
GITEA_TOKEN = os.environ.get('GITEA_TOKEN')

GITHUB_USER = os.environ.get('GITHUB_USER')

VERBOSE = os.environ.get('VERBOSE') or True

def mirroring(string):
    if VERBOSE:
        print("mirroring ... " + string)

def verbose(string):
    if VERBOSE:
        print(string)

def check_env():
    return None in [GITEA_HOST, GITEA_TOKEN, GITEA_UID, GITHUB_USER]

def mirror(origin):
    headers = {'content-type': 'application/json'}
    payload = {
        "clone_addr": origin['clone_url'],
        "description": origin['description'],
        "mirror": True,
        "private": False,
        "repo_name": origin['name'],
        "uid": GITEA_UID
    }
    mirroring(origin['clone_url'])
    response = requests.post('https://' + GITEA_HOST + '/api/v1/repos/migrate?token=' + GITEA_TOKEN,
        data=json.dumps(payload),
        headers=headers)

    if response.status_code == 201:
        mirroring('done')
    else:
        mirroring('failed')
    return


if __name__ == '__main__':
    if check_env():
        print("please export ENV variables and run")
        print("./mirror_github_stars.py")
        sys.exit(1)

    verbose("mirror starred github repositories from " + GITHUB_USER)
    page = 1
    while True:
        response = requests.get("https://api.github.com/users/" + GITHUB_USER + "/starred?page=" + str(page) + "&per_page=100")
        repos = response.json()
        if len(repos) > 0:
            for repo in repos:
                mirror(repo)
            page += 1
        else:
            break
