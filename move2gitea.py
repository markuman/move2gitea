#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
@author: m

# requirements

* python3
* GitPython
* requests
"""

import requests, sys, git, os, json

GITEA_HOST = os.environ.get('GITEA_HOST')
GITEA_UID = int(os.environ.get('GITEA_UID'))
GITEA_TOKEN = os.environ.get('GITEA_TOKEN')

GITHUB_USER = os.environ.get('GITHUB_USER')
GITHUB_PASSWORD = os.environ.get('GITHUB_PASSWORD')

VERBOSE = True

def migrating(string):
    if VERBOSE:
        print("migrating ... " + string)

def debug(string):
    if VERBOSE:
        print(string)

def check_env():
    return None in [GITEA_HOST, GITEA_TOKEN, GITEA_UID, GITHUB_USER, GITHUB_PASSWORD]

def migrate(origin):
    headers = {'content-type': 'application/json'}
    payload = {
        "auth_password": GITHUB_USER,
        "auth_username": GITHUB_PASSWORD,
        "clone_addr": origin['clone_url'],
        "description": origin['description'],
        "mirror": False,
        "private": origin['private'],
        "repo_name": origin['name'],
        "uid": GITEA_UID
    }
    migrating(origin['clone_url'])
    response = requests.post('https://' + GITEA_HOST + '/api/v1/repos/migrate?token=' + GITEA_TOKEN,
        data=json.dumps(payload),
        headers=headers)

    if response.status_code == 201:
        migrating('done')
    else:
        migrating('failed')
    return


if __name__ == '__main__':
    if check_env():
        print("please export ENV variables and run")
        print("./move2gitea.py")
        sys.exit(1)

    debug("migrating repositories from " + GITHUB_USER)
    page = 1
    while True:
        response = requests.get("https://api.github.com/users/" + GITHUB_USER + "/repos?page=" + str(page) + "&per_page=100")
        repos = response.json()
        if len(repos) > 0:
            for repo in repos:
                migrate(repo)
            page += 1
        else:
            break
